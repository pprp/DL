# NVIDIA 驱动安装

[挂载u盘https://www.cnblogs.com/dmj666/p/8031828.html](https://www.cnblogs.com/dmj666/p/8031828.html)

参考的博客：

[https://blog.csdn.net/stories_untold/article/details/78521925](https://blog.csdn.net/stories_untold/article/details/78521925)

```shell
# 学长拿出来的攻略
1. remove previous installations
sudo apt-get purge nvidia*
sudo apt-get autoremove
sudo dpkg -P cuda-repo-ubuntu1404
sudo dpkg -P cuda-repo-ubuntu1604

2. install dependencies
sudo apt-get install build-essential gcc-multilib dkms

3. create blacklist for nouveau driver
sudo gedit /etc/modprobe.d/blacklist-nouveau.conf
{
  blacklist nouveau
  options nouveau modeset=0
}

sudo update-initramfs -u
reboot

3. stop lightdm/gdm/kdm
sudo systemctl stop lightdm
sudo systemctl stop gdm
sudo systemctl stop kdm

4. Excuting the Runfile
cd ~ 
chmod +x NVIDIA-Linux-x86_64-375.66.run
sudo ./NVIDIA-Linux-x86_64-375.66.run --dkms

```

登陆nvidia官网，可以得到适合自己电脑的驱动，下载下来

####  删除原有驱动

```sudo apt-get remove --purge nvidia*```

#### 禁用nouveau驱动： 

编辑 /etc/modprobe.d/blacklist-nouveau.conf 文件，添加以下内容：

```
blacklist nouveau
blacklist lbm-nouveau
options nouveau modeset=0
alias nouveau off
alias lbm-nouveau off
```
 关闭nouveau：
```$ echo options nouveau modeset=0 | sudo tee -a /etc/modprobe.d/nouveau-kms.conf```

#### reboot

```
$ update-initramfs -u
$ sudo reboot
```

#### 获取kernel source （important）

```
$ apt-get install linux-source
$ apt-get install linux-headers-x.x.x-x-generic
```

其中x.x.x-x-generic可以通过$(uname -r)替换得到

#### 安装nvidia驱动
```sudo chmod NVIDIA*.run```

```sudo ./NVIDIA*.run```

安装过程中一些选项

The distribution-provided pre-install script failed! Are you sure you want to continue? 

选择 yes 继续。

Would you like to register the kernel module souces with DKMS? This will allow DKMS to automatically build a new module, if you install a different kernel later? 

选择 No 继续。

问题大概是：Nvidia's 32-bit compatibility libraries? 选择 No 继续。

Would you like to run the nvidia-xconfigutility to automatically update your x configuration so that the NVIDIA x driver will be used when you restart x? Any pre-existing x confile will be backed up.

选择 Yes  继续

#### 挂载Nvidia驱动

```modprobe nvidia```

#### 检查驱动是否安装成功

```nvidia-smi```

成功了，好激动，分享给大家，希望不要在这个上边浪费太长时间，迷茫太久






















